import numpy as np
import  matplotlib.pylab as plt

from numpy.linalg import norm
from scipy.ndimage.filters import convolve
from skimage import color, img_as_ubyte
from skimage.exposure import adjust_gamma
from skimage.util import view_as_windows, view_as_blocks
from skimage.transform import resize
from sklearn.svm import LinearSVC


def get_intensity(image):
    return color.rgb2ycbcr(image)[:, :, 0]


def get_derivative(image):
    x_diff = np.array([[-1, 0, 1]])
    y_diff = x_diff.T
    dx = convolve(image, x_diff)
    dy = convolve(image, y_diff)
    directions = np.arctan2(dy, dx)
    directions[directions < 0] += np.pi
    return np.sqrt(dx**2 + dy**2), np.degrees(directions)


def hist_by_cell(grad_cell, dir_cell, hist, index):
    for g, d in zip(grad_cell, dir_cell):
        hist_idx = int(min(np.floor(d / 20), 8))
        bin_center = hist_idx * 20 + 10
        if d - bin_center < 0:
            next_idx = (hist_idx - 1) % 9
            alpha = 1.0 - (bin_center - d) / 20
        else:
            next_idx = (hist_idx + 1) % 9
            alpha = 1.0 - (d - bin_center) / 20
        hist[index + hist_idx] += (1.0 - alpha) * g
        hist[index + next_idx] += alpha * g


def extract_hog(image):
    adjusted = img_as_ubyte(adjust_gamma(image, gamma=0.5))
    small = resize(adjusted, (64, 64), mode='reflect')
    grad, dirs = get_derivative(get_intensity(small))

    CELL_DIM = (8, 8)
    CELL_SIZE = CELL_DIM[0] * CELL_DIM[1]
    CELLS_DIM = (CELL_DIM[0] * CELL_DIM[0],) + CELL_DIM
    BINS_COUNT = 9
    BLOCK_DIM = (2, 2 * BINS_COUNT)
    BLOCK_STEP = (1, BINS_COUNT)
    HIST_SIZE = BINS_COUNT * grad.size // CELL_SIZE
    HOG_SIZE = 7 * 7 * 4 * BINS_COUNT
    EPSILON = np.float64(10**(-50))

    hist = np.zeros(HIST_SIZE)
    grad_cells = view_as_blocks(grad, CELL_DIM).reshape(CELLS_DIM)
    dir_cells = view_as_blocks(dirs, CELL_DIM).reshape(CELLS_DIM)
    i = 0
    for grad_cell, dir_cell in zip(grad_cells, dir_cells):
        g = grad_cell.reshape(CELL_SIZE)
        d = dir_cell.reshape(CELL_SIZE)
        hist_by_cell(g, d, hist, i)
        i += BINS_COUNT
    hist = hist.reshape((CELL_DIM[0], -1))
    hog = np.ndarray(HOG_SIZE)
    i = 0
    for block in view_as_windows(hist, BLOCK_DIM, BLOCK_STEP):
        vec = block.reshape(-1)
        vec_norm = max(norm(vec, ord=2), EPSILON)
        size = vec.size
        hog[i:i + size] = vec / vec_norm
        i += size
    return hog


def fit_and_classify(X_train, y_train, X_test):
    svm = LinearSVC(multi_class='crammer_singer', random_state=42)
    svm.fit(X_train, y_train)
    return svm.predict(X_test)
